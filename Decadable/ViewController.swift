//
//  ViewController.swift
//  Decadable
//
//  Created by Ezaden Seraj on 23/11/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit

struct WebstieDescription: Decodable {
    let name: String
    let description: String
    let courses: [Course]
}

struct Course: Decodable {
    let id: Int
    let name: String
    let link: String
    let imageUrl: String
    
    init(json: [String: Any]) {
        id = json["id"] as? Int ?? -1
        name = json["name"] as? String ?? ""
        link = json["link"] as? String ?? ""
        imageUrl = json["imageUrl"] as? String ?? ""
    }
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let jsonUrlString2 = "https://api.letsbuildthatapp.com/jsondecodable/website_description"
        let jsonUrlString = "https://api.letsbuildthatapp.com/jsondecodable/course"
        
        guard let url = URL(string: jsonUrlString2) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {return}
            
            do {
//                let course = try JSONDecoder().decode(Course.self, from: data)
//                print(course.name)
                let websiteDescription = try JSONDecoder().decode(WebstieDescription.self, from: data)
                print(websiteDescription.name)
            } catch let jsonError {
                print("Json Error: ", jsonError)
            }
            
        }.resume()
        
        
        
    }

   


}

